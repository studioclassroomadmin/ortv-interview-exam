<?php
/**
 * DB able: MEMBERS
 * 欄位有: 
 * SN: 流水號
 * USERNAME: EMAIL帳號
 * MOBILE: 手機
 * CHINESE_NAME: 中文姓名
 * 
 * 
 * 題目1：請參閱如下範例, 用PHP寫出SQL語法中的 新增/刪除/修改。
 * 
 */

// DB Connection
include '/var/www/html/mshop/db.php';

function getMemberInfo($email){
    $result = false;
    $db = getMemberDB();
    $stmt = $db->prepare("SELECT * FROM MEMBERS  WHERE USERNAME = ?");
    $stmt->execute(array($email));
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $count = $stmt->rowCount();
    if($count>0)
        $result = $data[0];
    return $result;
}
$memberInfo = getMemberInfo($_SESSION["email"]);



?>